<?php
$doc_root = preg_replace("!${_SERVER['SCRIPT_NAME']}$!", '', $_SERVER['SCRIPT_FILENAME']);
require_once($doc_root . '/wp-load.php');
add_filter('wp_mail_content_type', 'set_html_mail');

$postData = $uploadedFile = $statusMsg = '';
$msgClass = 'errordiv';

$postData = $_POST;
$email = $_POST['email'];
$name = $_POST['name'];
$weight = $_POST['weight'];

// Do we have data?
if (!empty($email) && !empty($name) && !empty($weight)) {
    echo "We have data";
    // Valid email?
    if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
        $statusMsg = __('Please enter a valid email-address', 'sage');
    } else {
        $uploadStatus = 1;

        // Upload attachment
        if (!empty($_FILES['picture']['name'])) {
            $targetDir = 'attachments/';
            $fileName = basename($_FILES['attachment']['name']);
            $targetFilePath = $targetDir . $fileName;
            $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);

            // Another check for filetypes
            $allowTypes = [
                'jpg',
                'jpeg',
                'jpeg',
                'jpeg',
                'png',
                'gif',
            ];
            if (in_array($fileType, $allowTypes)) {
                if (move_uploaded_file($_FILES['attachment']['temp_name'], $targetFilePath)) {
                    $uploadedFile = $targetFilePath;
                } else {
                    $uploadStatus = 0;
                    $statusMsg = __('Sorry, there was an error uploading your file', 'sage');
                }
            } else {
                $uploadStatus = 0;
                $statusMsg = 'Sorry, only ' . implode(', ', $allowTypes) . ' are allowed';
            }
        }

        if ($uploadStatus == 1) {
            // Recipient
            $toEmail = $_POST['toEmail'];

            // Sender
            $fromEmail = $_POST['email'];
            $fromName = $_POST['name'] . ', ' . $_POST['company'];

            // Subject
            $emailSubject = 'Offerte ' . $_POST['weight'] . 'kg ' . $_POST['material-type'];

            $htmlContent = "<h4>{$emailSubject}</h4>
            <ul style=\"list-style-type: none;\">
                <li>Naam: {$fromName}</li>
                <li>Email: {$fromEmail}</li>
                <li>Telefoon: {$_POST['phone']}</li>
                <li>Adres: {$_POST['address']}</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp; {$_POST['zip']} {$_POST['city']}</li>
                <li>BTW: {$_POST['tax']}</li>
                <li>KVK: {$_POST['kvk']}</li>
            </ul>
            {$_POST['weight']} kg {$_POST['material-type']} à €{$_POST['price']}";

            // Headers
            $headers = "From: {$fromName}<{$fromEmail}>";

            if (!empty($uploadedFile) && file_exists($uploadedFile)) {
                // Boundary
                $semi_rand = md5(time());
                $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";

                // Headers for attachment
                $headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"";

                // Multipart boundary
                $message = "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"UTF-8\"\n" . "Content-Transfer-Encoding: 7bit\n\n" . $htmlContent . "\n\n";

                // Preparing attachment
                if (is_file($uploadedFile)) {
                    $message .= "--{$mime_boundary}\n";
                    $fp =    @fopen($uploadedFile, "rb");
                    $data =  @fread($fp, filesize($uploadedFile));
                    @fclose($fp);
                    $data = chunk_split(base64_encode($data));
                    $message .= "Content-Type: application/octet-stream; name=\"" . basename($uploadedFile) . "\"\n" .
                        "Content-Description: " . basename($uploadedFile) . "\n" .
                        "Content-Disposition: attachment;\n" . " filename=\"" . basename($uploadedFile) . "\"; size=" . filesize($uploadedFile) . ";\n" .
                        "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
                }

                $message .= "--{$mime_boundary}--";
                $returnpath = "-f" . $fromEmail;

                $mail = mail($toEmail, $emailSubject, $message, $headers, $uploadedFile);
                @unlink($uploadedFile);
            } else {
                // Set content-type header for sending HTML email
                $headers .= "\r\n" . "MIME-Version: 1.0";
                $headers .= "\r\n" . "Content-type:text/html;charset=UTF-8";

                // Send email
                echo "$toEmail, $emailSubject, $htmlContent, $headers";
                $mail = mail($toEmail, $emailSubject, $htmlContent, $headers);
            }

            // If mail sent
            if ($mail) {
                $statusMsg = 'Your contact request has been submitted successfully !';
                $msgClass = 'succdiv';

                $postData = '';
            } else {
                $statusMsg = 'Your contact request submission failed, please try again.';
            }
        }
        remove_filter('wp_mail_content_type', 'set_html_mail');

        function set_html_mail()
        {
            return 'text/html';
        }
    }
}

header("Location: /succes");
