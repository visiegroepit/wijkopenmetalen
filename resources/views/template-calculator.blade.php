{{--
  Template Name: Met calculator
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.calculator')
    @include('partials.content-page')
  @endwhile
@endsection
