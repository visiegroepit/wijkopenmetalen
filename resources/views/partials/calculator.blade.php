<?php 
global $wp;
$current_url = add_query_arg( $wp->query_vars, home_url( $wp->request ) );
if(isset($_POST['Submit'])) {
  $name = $_POST['name'];
  $price50 = $_POST['price50'];
  $price300 = $_POST['price300'];
  $price500 = $_POST['price500'];
  ?>
<script>
  var name = {{ $name }};
  var price50 = {{ $price50 }};
  var price300 = {{ $price300 }};
  var price500 = {{ $price500 }};
  alert(name);
</script>
<?php
} ?>

<script src="https://vuejs.org/js/vue.js"></script>
@if(have_rows('material_types'))
<section class="calculator container">
  <div class="row">
    <?php ($materials=get_field('material_types')); ?>
    @foreach ($materials as $material)
    <div class="col-6 col-md-4 col-xl-3">
      <div class="material-type-container border" id="calculator">
        <div class="col-12 material-type-image" style="background-image: url({{ $material['material_type_picture'] }})">
        </div>
        <span>{{$material['material_type_name'] }}</span>
        <table class="table">
          <tr class="row">
            <td class="col-5 offset-1">50+kg</td>
            <td class="col-5">€ {{$material['material_type_price50'] }}</td>
          </tr>
          <tr class="row">
            <td class="col-5 offset-1">300+kg</td>
            <td class="col-5">€ {{$material['material_type_price300'] }}</td>
          </tr>
          <tr class="row">
            <td class="col-5 offset-1">500+kg</td>
            <td class="col-5">€ {{$material['material_type_price500'] }}</td>
          </tr>
        </table>
        <form method="post" action="<?php echo get_template_directory_uri(); ?>/materialcalculator.php">
          <input type="hidden" name="name" value="{{$material['material_type_name'] }}">
          <input type="hidden" name="price50" value="{{$material['material_type_price50'] }}">
          <input type="hidden" name="price300" value="{{$material['material_type_price300'] }}">
          <input type="hidden" name="price500" value="{{$material['material_type_price500'] }}">
          <input type="submit" name="Submit" class="btn btn-secondary calculate-button" value="Berekenen">
        </form>
      </div>
    </div>
    @endforeach
  </div>
</section>
@endif