<div class="page-header container-fluid"
  style="background-image: url({{get_field('material_haeder_image')}});">
  <div class="overlay">
  </div>
  <h1 class="page-header">{{ the_title() }}<span>
  <div class="shape-bottom">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
      <path class="elementor-shape-fill" d="M500.2,94.7L0,0v100h1000V0L500.2,94.7z"></path>
    </svg>
  </div>
</div>
