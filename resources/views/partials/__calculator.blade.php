<script src="https://vuejs.org/js/vue.js"></script>
<section id="calculator" class="calculator container">
  @verbatim
  <div class="row text-center">
    <div class="col-12 col-sm-6 col-xl-3 my-3" v-for="(material, i) in materials">
      <div class="material-type-container border">
        <div class="col-12 material-type-image"
          :style="{ backgroundImage: 'url(' + material.material_type_picture + ')' }">
        </div>
        <span class="material-type">{{material.material_type_name}}</span>
        <table class="table">
          <tr class="row">
            <td class="col-5 offset-1">50+kg</td>
            <td class="col-5">€ {{parseFloat(material.material_type_price50).toFixed(2)}}</td>
          </tr>
          <tr class="row">
            <td class="col-5 offset-1">300+kg</td>
            <td class="col-5">€ {{parseFloat(material.material_type_price300).toFixed(2)}}</td>
          </tr>
          <tr class="row">
            <td class="col-5 offset-1">500+kg</td>
            <td class="col-5">€ {{parseFloat(material.material_type_price500).toFixed(2)}}</td>
          </tr>
        </table>
        <button type="button" class="btn btn-secondary calculate-button" data-toggle="modal"
        data-target="#calculatormodal">Bereken</button>
        <div class="modal fade" id="calculatormodal" tabindex="-1" role="dialog" aria-labelledby="calculatorModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 id="calculatorModalLabel" class="modal-title">{{material.material_type_name}}</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Sluiten">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <table class="table">
                    <tr>
                      <th>50+ kg</th>
                      <th>300+ kg</th>
                      <th>500+ kg</th>
                    </tr>
                    <tr>
                      <td>€ {{ parseFloat(material.material_type_price50).toFixed(2) }}</td>
                      <td>€ {{ parseFloat(material.material_type_price300).toFixed(2) }}</td>
                      <td>€ {{ parseFloat(material.material_type_price500).toFixed(2)  }}</td>
                    </tr>
                  </table>
                  <form id="offerte" method="post" action="<?php echo get_template_directory_uri(); ?>/calculatormailer.php">
                    <input type="hidden" name="url" value="<?php echo $_SERVER["REQUEST_URI"]; ?>" />
                    <input type="hidden" name="material-type" v-model="material.material_type_name" />
                    <div class="input-group">
                      <input id="weight" name="weight" type="number" v-model="weight" placeholder="Gewicht" step="0.1"
                        class="form-control" required>
                      <div class="input-group-append">
                        <span class="input-group-text">kg</span>
                      </div>
                    </div>
                    <span class="calculated-price" v-if="weight >= 50 && weight < 300">€
                      {{ parseFloat(material.material_type_price50 * weight).toFixed(2) }}</span>
                    <span class="calculated-price" v-else-if="weight >=300 && weight < 500">€
                      {{ parseFloat(material.material_type_price300 * weight).toFixed(2) }}</span>
                    <span class="calculated-price" v-else-if="weight >= 500">€
                      {{ parseFloat(material.material_type_price500 * weight).toFixed(2) }}</span>
                    <span class="calculated-price" v-else>€ {{ parseFloat(0).toFixed(2) }}</span>
                    <input type="hidden" name="price" v-if="weight >= 50 && weight < 300"
                      v-model="parseFloat(material.material_type_price50 * weight).toFixed(2)">
                    <input type="hidden" name="price" v-else-if="weight >=300 && weight < 500"
                      v-model="parseFloat(material.material_type_price300 * weight).toFixed(2)">
                    <input type="hidden" name="price" v-else-if="weight >= 500"
                      v-model="parseFloat(material.material_type_price500 * weight).toFixed(2)">
                    <input type="hidden" name="price" v-else v-model="parseFloat(0).toFixed(2)">
                    <br>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="radioOptions" id="particulier"
                        value="particulier" v-model="radio">
                      <label class="form-check-label" for="particulier">Particulier</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="radioOptions" id="bedrijf" value="bedrijf"
                        v-model="radio">
                      <label class="form-check-label" for="bedrijf">Bedrijf</label>
                    </div>
                    <div class="form-group" id="contact-bedrijf" v-if="radio=='bedrijf'">
                      <input type="text" class="form-control" name="company" placeholder="Bedrijfsnaam">
                      <input type="text" class="form-control" name="name" placeholder="Naam contactpersoon">
                      <input type="email" class="form-control" name="email" placeholder="Email-adres">
                      <input type="tel" class="form-control" name="phone" placeholder="Telefoonnummer">
                      <input type="text" class="form-control" name="address" placeholder="Adres">
                      <input type="text" class="form-control" name="zip" placeholder="Postcode">
                      <input type="text" class="form-control" name="city" placeholder="Woonplaats">
                      <input type="text" class="form-control" name="tax" placeholder="BTW-nummer">
                      <input type="text" class="form-control" name="kvk" placeholder="KVK-nummer">
                    </div>
                    <div class="form-group" id="contact-particulier" v-if="radio=='particulier'">
                      <input type="text" class="form-control" name="name" placeholder="Naam">
                      <input type="text" class="form-control" name="address" placeholder="Adres">
                      <input type="text" class="form-control" name="zip" placeholder="Postcode">
                      <input type="text" class="form-control" name="city" placeholder="Woonplaats">
                      <input type="email" class="form-control" name="email" placeholder="Email-adres">
                      <input type="tel" class="form-control" name="phone" placeholder="Telefoonnummer">
                    </div>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="hidden" name="MAX_FILE_SIZE" value="524288" />
                        <input type="file" class="custom-file-input" id="picture" name="picture" placeholder="Foto upload"
                          accept="image/*" capture="camera" required>
                        <label class="custom-file-label" for="picture">Kies foto (max 512kb)</label>
                      </div>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" name="terms-conditions" required>
                      <label class="form-check-label" for="terms-conditions">Ik accepteer de <a
                          href="/Privacybeleid">Algemene Voorwaarden</a></label>
                    </div>
                    <div class="form-group">
                      <input type="submit" value="Versturen" class="btn btn-secondary mt-3">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
  
  <?php if(!empty($statusMsg)){ ?>
  <p class="statusMsg <?php echo !empty($msgClass)?$msgClass:''; ?>"><?php echo $statusMsg; ?></p>
  <?php } ?>
</section>


<script>
  var materials = <?php echo json_encode(get_field('material_types')); ?>;
  let calculator = new Vue({
    el: '#calculator',
    data: {
      materials: materials,
      weight: '',
      radio: '',
      material: [],
      material: {
        material_type_picture: '/wp-content/uploads/2019/10/cropped-Logo-v2.png',
      }
    },
  });

</script>
@endverbatim
